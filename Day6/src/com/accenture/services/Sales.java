package com.accenture.services;

import com.accenture.product.Product;
import com.accenture.inventory.*;
public interface Sales {
	public double find_avg();
	public boolean pricedip(Product product);
	public boolean editprice(int pid, double price);
	
}
