package com.accenture.services;

import java.util.Iterator;

import com.accenture.inventory.*;
import com.accenture.product.Product;

public class Sales_implements implements Sales {
	Inv productList = new Inv_Implements();
	@Override
	public boolean pricedip(Product product) {
		// TODO Auto-generated method stub
		if(product.getPrice()<find_avg())
			return true;
		return false;
	}

	@Override
	public boolean editprice(int pid, double price) {
		// TODO Auto-generated method stub
		Iterator<Product> pr = productList.displayAll().iterator();
		while(pr.hasNext()) {
			Product temp = pr.next();
			if(temp.getPid() == pid) {
				temp.setPrice(price);
				return true;
			}
		}
		return false;
	}

	@Override
	public double find_avg() {
		// TODO Auto-generated method stub
		double average=0;
		int count = 0;
		Iterator<Product> pr = productList.displayAll().iterator();
		while(pr.hasNext()) {
			Product temp = pr.next();
			average += temp.getPrice();
			count++;
			}
		return (average/count);
	}

}
