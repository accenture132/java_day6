package com.accenture.inventory;

import java.util.*;

import com.accenture.product.Product;

public class Inv_Implements implements Inv{

	private List<Product> productList = new ArrayList<>();

	@Override
	public boolean add(Product products) {
		// TODO Auto-generated method stub
		return productList.add(products);
	}
	
	@Override
	public List<Product> displayAll() {
		// TODO Auto-generated method stub
		return productList;
	}

	@Override
	public boolean delete(int pid) {
		// TODO Auto-generated method stub
		Iterator<Product> pr = productList.iterator();
		while(pr.hasNext()) {
			Product temp = pr.next();
			if(temp.getPid()==pid) {
				pr.remove();
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean update(int pid, Product product) {
		// TODO Auto-generated method stub
		Iterator<Product> pr = productList.iterator();
		while(pr.hasNext()) {
			Product temp = pr.next();
			if(temp.getPid()==pid) {
				temp.setPrice(product.getPrice());
				temp.setName(product.getName());
				temp.setQuantity(product.getQuantity());
				return true;
			}
		}
		return false;
	}
	
	
}
