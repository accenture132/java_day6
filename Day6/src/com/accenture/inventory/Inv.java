package com.accenture.inventory;

import java.util.*;
import com.accenture.product.*;

public interface Inv {
	public boolean add(Product products);
	public List<Product> displayAll();
	public boolean delete(int pid);
	public boolean update(int pid, Product product);
}
